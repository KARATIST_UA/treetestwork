<?php


class Tree
{
    private $treeArray = [];
    private $freeLeaves = [];
    private $maxWeight;

    const NODE = 'nodes';
    const LEAF = 'lists';

    function __construct($treeArray = [], $maxWeight)
    {
        $this->treeArray = $treeArray;
        $this->maxWeight = $maxWeight;
        $this->parseTree();
    }

    private function parseTree()
    {
        $this->treeArray = $this->parseNode($this->treeArray);
    }

    private function parseNode($tree)
    {
        for ($i=0; $i<count($tree); $i++) {
            $tree[$i][self::LEAF] = $this->deleteLeaves($tree[$i][self::LEAF]);
            if ($tree[$i][self::NODE]) {
                $tree[$i][self::NODE] = $this->parseNode($tree[$i][self::NODE]);
            }
        }

        return $tree;
    }

    private function sortLeaves($leaves = [])
    {
        $count = count($leaves);
        for ($i = 0; $i <= $count-1; $i++) {
            for ($j = $count - 1; $j > $i; $j--) {
                if ($leaves[$j-1] > $leaves[$j]) {
                    $temp = $leaves[$j-1];
                    $leaves[$j-1] = $leaves[$j];
                    $leaves[$j] = $temp;
                }
            }
        }
        return $leaves;
    }
    
    private function deleteLeaves($leaves = [])
    {
        $leaves = $this->sortLeaves(array_merge($this->freeLeaves, $leaves));
        $totalWeight = array_shift($leaves);

        if ($totalWeight > $this->maxWeight) {
            $this->freeLeaves = $leaves;
            return [];
        }

        $newLeaves = [$totalWeight];
        $this->freeLeaves = [];

        for ($i = 0; $i < count($leaves); $i++) {
            if($totalWeight + $leaves[$i] <= $this->maxWeight) {
                $newLeaves[] = $leaves[$i];
            }
            else {
                $this->freeLeaves[] = $leaves[$i];
            }
        }
        return $newLeaves;
    }

    public function getTree()
    {
        return $this->treeArray;
    }

    public function getFreeLeaves()
    {
        return $this->freeLeaves;
    }
}